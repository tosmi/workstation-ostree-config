#!/bin/bash

set -euf -o pipefail

PATH=/bin:/usr/bin:/sbin:/usr/sbin

LAST_COMMIT=$(ostree log --repo=repo fedora/31/x86_64/stderr-gnome | awk '/^commit / {print $2; exit}')
if [ -z "$LAST_COMMIT" ]; then
  echo "Could not get last commit!"
  exit 1
fi

if ! ostree gpg-sign --repo=repo "$LAST_COMMIT" A17FAD6DEBDC7B3D78407B9CF9877D9C57F9CFBF; then
  echo "ostree gpg-sign failed!"
  exit 1
fi

if ! ostree summary --repo=repo --update; then
  echo "ostree summary update failed!"
  exit 1
fi
