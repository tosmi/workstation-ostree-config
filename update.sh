#!/bin/bash

set -euf -o pipefail

PATH=/usr/bin:/usr/sbin

FEDORA_VERSION=31
IMAGE=quay.io/fedora/fedora:"$FEDORA_VERSION"-x86_64
PODMAN_EXEC="sudo podman exec -ti -w /data/workstation-ostree-config silverblue"

create_container() {
  echo "creating new silverblue container"
  sudo podman create \
              --hostname silverblue \
              --name silverblue \
              --network host \
              --pid host \
              --privileged \
              --user root:root \
              --volume /data:/data \
              --volume /var/tmp:/var/tmp \
              $IMAGE \
              sleep +Inf

#               --security-opt label=disable,seccomp=unconfined \

  sudo podman start silverblue

  echo "installing required packages"
  $PODMAN_EXEC dnf install -q -y rpm-ostree ostree strace
}

container_exists?() {
  ID=$(sudo podman ps -qaf name=silverblue)
  
  [ -z "$ID" ] && return 1

  return 0
}

[ ! -d repo/config ] && ostree --repo=repo init --mode=archive

container_exists? || create_container

if ! $PODMAN_EXEC rpm-ostree compose tree --repo=repo --cachedir=cache fedora-stderr-gnome.yaml; then
  echo "rpm-ostree compose failed!"
  exit 1
fi 
